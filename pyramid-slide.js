const pyramidHeight = document.getElementById("pyramid-height");
const pyramidHeightValue = document.getElementById("pyramid-height-value");
const pyramidStyle = document.getElementById("pyramid-style");

const getStyle = () => {
    return pyramidStyle.options[pyramidStyle.selectedIndex].text;
};

const getHeight = () => {
    return parseInt(pyramidHeight.value);
};

const drawPyramidWithText = (height, style) => {
    const pyramid = document.getElementById("pyramid");
    pyramid.innerHTML = "";

    for (let i = 2; i < height + 2; i++) {
        let row = document.createElement("p");
        row.appendChild(document.createTextNode('\u00A0'.repeat(height + 1 - i) + style.repeat(i)));
        pyramid.appendChild(row);
    }
};

pyramidHeightValue.innerText = getHeight();
drawPyramidWithText(getHeight(), getStyle());

const pyramidHeightListener = event => {
    pyramidHeightValue.innerText = getHeight();
    drawPyramidWithText(getHeight(), getStyle());
};

const blockStyleListener = event => {
    drawPyramidWithText(getHeight(), getStyle());
};

pyramidStyle.addEventListener("change", blockStyleListener);
pyramidHeight.addEventListener("mousemove", pyramidHeightListener);


const drawPyramid = height => {
    const pyramid = document.getElementById('pyramid');
    pyramid.innerHTML = "";

    for (let i = 2; i < height + 2; i++) {
        let row = document.createElement('div');

        for (let j = 0; j < height + 1 - i; j++) {
            let emptyBlock = document.createElement('div');
            emptyBlock.setAttribute('class', 'emptyBlock');
            row.appendChild(emptyBlock);
        }
        for (let j = 0; j < i; j++) {
            let block = document.createElement('div');
            block.setAttribute('class', 'block');
            row.appendChild(block);
        }

        pyramid.appendChild(row);
    }
    const construction = document.getElementById('construction');
    construction.remove();
};


