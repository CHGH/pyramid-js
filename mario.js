
// window.onbeforeunload = function() {
//     return "There are unsaved changes. Leave now?";
// };

printPyramid(5);

function printPyramid(height) {
    let blocks = 2;
    let spaces = height - 1;
    for (let i = 0; i < height; i++) {
        console.log(" ".repeat(spaces) + "#".repeat(blocks));
        blocks++;
        spaces--;
    }
}

const drawPyramid = height => {
    const pyramid = document.getElementById('pyramid');
    for (let i = 2; i < height + 2; i++) {
        let row = document.createElement('div');

        for (let j = 0; j < height + 1 - i; j++) {
            let emptyBlock = document.createElement('div');
            emptyBlock.setAttribute('class', 'emptyBlock');
            row.appendChild(emptyBlock);
        }
        for (let j = 0; j < i; j++) {
            let block = document.createElement('div');
            block.setAttribute('class', 'block');
            row.appendChild(block);
        }

        pyramid.appendChild(row);
    }
    const construction = document.getElementById('construction');
    construction.remove();
};

drawPyramid(10);

const clickMe = document.getElementById("clickMe");
const onClick = () => { console.log("Button clicked"); };
clickMe.addEventListener("click", onClick);

const clickMeAny = document.getElementById("clickMeAny");
const onAnyClick = (event) => {
    if (event.which == 1) console.log("Left button.");
    else if (event.which == 2) console.log("Middle button.");
    else if (event.which == 3) console.log("Right button.");
    else console.log(event);
};
clickMeAny.addEventListener("mousedown", onAnyClick);

window.addEventListener("keydown", function(event) {
    if (event.keyCode == 86)
        document.body.style.background = "violet";
});
window.addEventListener("keyup", function(event) {
    if (event.keyCode == 86)
        document.body.style.background = "";
});
window.addEventListener("keypress", function(event) {
    console.log(String.fromCharCode(event.charCode));
});

// window.addEventListener("click", function(event) {
//     const dot = document.createElement("div");
//     dot.className = "dot";
//     dot.style.left = (event.pageX - 4) + "px";
//     dot.style.top = (event.pageY - 4) + "px";
//     document.body.appendChild(dot);
// });

let lastX; // Tracks the last observed mouse X position
const rect = document.getElementById("dragDiv");
rect.addEventListener("mousedown", function(event) {
    if (event.which == 1) {
        lastX = event.pageX;
        window.addEventListener("mousemove", moved);
        event.preventDefault(); // Prevent selection
    }
});

function buttonPressed(event) {
    if (event.buttons == null)
        return event.which != 0;
    else
        return event.buttons != 0;
}
function moved(event) {
    if (!buttonPressed(event)) {
        window.removeEventListener("mousemove", moved);
    } else {
        const dist = event.pageX - lastX;
        const newWidth = Math.max(10, rect.offsetWidth + dist);
        console.log("New width: " + newWidth);
        rect.style.width = newWidth + "px";
        lastX = event.pageX;
    }
}

const para = document.getElementById("paraHover");
function isInside(node, target) {
    for (; node != null; node = node.parentNode)
        if (node == target) {
            return true;
        }
}
para.addEventListener("mouseover", function(event) {
    if (!isInside(event.relatedTarget, para)) {
        para.style.color = "red";
    }
});
para.addEventListener("mouseout", function(event) {
    if (!isInside(event.relatedTarget, para)) {
        para.style.color = "";
    }
});


const help = document.querySelector("#help");
const fields = document.querySelectorAll("input");
for (var i = 0; i < fields.length; i++) {
    fields[i].addEventListener("focus", function(event) {
        const text = event.target.getAttribute("data-help");
        help.textContent = text;
    });
    fields[i].addEventListener("blur", function(event) {
        help.textContent = "";
    });
}

const link = document.querySelector("a");
link.addEventListener("click", function(event) {
    console.log("Nope.");
    event.preventDefault();
});

document.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM is ready!");
});

